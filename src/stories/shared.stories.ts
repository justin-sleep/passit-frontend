import { NgrxFormsModule } from "ngrx-forms";
import { InlineSVGModule } from "ng-inline-svg";
import { StoreModule } from "@ngrx/store";
import { HttpClientModule } from "@angular/common/http";
import { storiesOf, moduleMetadata } from "@storybook/angular";
import {
  withKnobs,
  boolean,
  text,
  select,
  object
} from "@storybook/addon-knobs";
import { RouterTestingModule } from "@angular/router/testing";

import { TextLinkComponent } from "../app/shared/text-link/text-link.component";
import { AsideLinkComponent } from "../app/shared/aside-link/aside-link.component";
import { TextFieldComponent } from "../app/shared/text-field/text-field.component";
import { HeadingComponent } from "../app/shared/heading/heading.component";
import { MultiselectComponent } from "../app/shared/multiselect/multiselect.component";
import { MultiselectListComponent } from "../app/shared/multiselect/multiselect-list/multiselect-list.component";
import { MultiselectAddComponent } from "../app/shared/multiselect/multiselect-add/multiselect-add.component";
import { BadgeComponent } from "../app/shared/multiselect/badge/badge.component";
import { FormLabelComponent } from "../app/shared/form-label/form-label.component";
import { CheckboxComponent } from "../app/shared/checkbox/checkbox.component";
import { StorybookModule } from "./storybook.module";

storiesOf("Shared", module)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [
        RouterTestingModule,
        NgrxFormsModule,
        HttpClientModule,
        InlineSVGModule.forRoot(),
        StoreModule.forRoot({}),
        StorybookModule
      ],
      declarations: [
        AsideLinkComponent,
        FormLabelComponent,
        HeadingComponent,
        TextFieldComponent,
        TextLinkComponent,
        MultiselectComponent,
        MultiselectListComponent,
        MultiselectAddComponent,
        BadgeComponent,
        CheckboxComponent
      ]
    })
  )
  .add("Marketing Frame", () => ({
    template: `<app-marketing-frame></app-marketing-frame>`
  }))
  .add("Text Link", () => {
    const defaultCaretState = "right";
    const caretStates = {
      None: "",
      Right: defaultCaretState,
      Left: "left"
    };
    // I think I need to use the any type because storybook is 4.0 but types aren't
    const caretControl: any = select(
      "Label State",
      caretStates,
      defaultCaretState
    );

    return {
      template: `
        <app-text-link
          caret="${caretControl}"
          [isMinor]="${boolean("Is Minor", false)}"
        >This is a text link.</app-text-link>
        <hr style="margin: 40px 0" />
        <app-text-link>
          Text link uses ng-content so you can <em>include</em>&nbsp;<strong>markup</strong>.
          Don't put elements that use display: block inside, though. It'll error!
        </app-text-link>
        <br /><br />
        <app-text-link>
          <em>Another sample with markup</em>
        </app-text-link>
        <h3>
          <app-text-link>
            This sample wraps app-text-link with a h3 tag
          </app-text-link>
        </h3>
        <app-text-link caret="right">
          Don't end a link with a period AND use a caret
        </app-text-link>
        &nbsp;&nbsp;&nbsp;
        <app-text-link caret="right">
          <del>This is bad.</del>
        </app-text-link>
        &nbsp;&nbsp;&nbsp;
        <app-text-link>
          Do this.
        </app-text-link>
        &nbsp;&nbsp;or&nbsp;&nbsp;
        <app-text-link caret="right">
          Or do this
        </app-text-link>
        <br /><br />
        When making <app-text-link>inline links</app-text-link>, never use the caret.
        <br /><br />
        <div style="font-size: 20px; font-weight: 400;">
          <app-text-link>
            Once you get over 19px, the link will inherit the font-weight rather than be set to 600.
          </app-text-link>
        </div>
        <div style="font-size: 20px; font-weight: 600;">
          <app-text-link>
            This one is inheriting the bold.
          </app-text-link>
        </div>
        <br /><br />
        This component is built with internal routerLinks in mind. If you want to go to an
        <app-text-link link="https://bing.com" [isExternal]="true">
          outside site
        </app-text-link>, add [isExternal]="true".
        <p>
          <app-text-link caret="right">Right Caret</app-text-link> |
          <app-text-link caret="left">Left Caret</app-text-link> |
          <app-text-link>No Caret</app-text-link>
        </p>
        <p>
          <app-text-link [isMinor]="true">"Minor" styling</app-text-link>
        </p>
        <p>
          Smart bold is Brendan leveraging a component to do more consistent styling. Shouldn't need
          to worry about it; it's on by default. But here's what it's doing:
        </p>
        <p style="font-weight: 400;">
          Here is a sample paragraph with <app-text-link>Smart bold on</app-text-link> and
          <app-text-link [smartBold]="false">Smart bold off</app-text-link>. Paragraph font weight is 400.
        </p>
        <p style="font-weight: 600;">
          Here is a sample paragraph with <app-text-link>Smart bold on</app-text-link> and
          <app-text-link [smartBold]="false">Smart bold off</app-text-link>. Paragraph font weight is 600.
        </p>
        <p style="font-weight: 700;">
          Here is a sample paragraph with <app-text-link>Smart bold on</app-text-link> and
          <app-text-link [smartBold]="false">Smart bold off</app-text-link>. Paragraph font weight is 700.
          (No changes - we don't have anything weightier than 700 at the moment!)
        </p>
        <p style="font-weight: 400; font-size: 20px;">
          Once you get over 19px font size, smart bold turns off and the link will always inherit the style.
          <app-text-link>Smart bold on</app-text-link> and <app-text-link [smartBold]="false">Smart bold
          off</app-text-link>. Paragraph font weight is 400, size is 20px.
        </p>
        <p>
          The component is managing underline thickness now too. I don't think I need to say more than that
          or have it be configurable.
        </p>
      `
    };
  })
  .add("Aside Link", () => ({
    template: `
      <aside-link>
        Here's an aside <em>link</em>!
      </aside-link>
    `
  }))
  .add("Text Input", () => ({
    template: `
      <app-text-field
        [ngrxFormControl]="ngrxFormControl"
        [label]="label"
        [isComplete]="isComplete"
        [inlineError]="inlineError"
        [type]="type"
      ></app-text-field>
    `,
    props: {
      ngrxFormControl: object("form control", {
        id: "test",
        value: "",
        errors: {},
        isSubmitted: false
      }),
      label: text("label", "My Field"),
      isComplete: boolean("isComplete", false),
      inlineError: text("inline error", ""),
      type: select(
        "type",
        { "": null, email: "email", password: "password", url: "url" } as any,
        undefined as any
      )
    }
  }))
  .add("Heading", () => ({
    template: `<app-heading [text]="text"></app-heading>`,
    props: {
      text: "Some Big Heading"
    }
  }))
  .add("Badge", () => {
    const defaultColor = "onahau";
    const colors = {
      None: "",
      Onahau: defaultColor,
      Buttermilk: "buttermilk"
    };
    const colorControl: any = select("Badge Color", colors, defaultColor);

    const defaultText = "Admin";
    const texts = {
      None: "",
      Admin: defaultText,
      Pending: "Pending",
      Whatever: "Whatever you want!"
    };
    const textControl: any = select("Badge Text", texts, defaultText);

    return {
      props: {
        color: colorControl,
        text: textControl
      },
      template: `
        <app-badge
          [color]="color"
          [text]="text"
        ></app-badge>
      `
    };
  })
  .add("SVG icons", () => {
    return {
      template: `
        <div style="display: flex; flex-wrap: wrap; padding: 0 20px;">
          <app-svg-demo iconId="#icon-autofill"></app-svg-demo>
          <app-svg-demo iconId="#icon-copy-username"></app-svg-demo>
          <app-svg-demo iconId="#icon-copy-password"></app-svg-demo>
          <app-svg-demo iconId="#icon-check"></app-svg-demo>
          <app-svg-demo iconId="#icon-external-link"></app-svg-demo>
          <app-svg-demo iconId="#icon-edit"></app-svg-demo>
          <app-svg-demo iconId="#icon-trash"></app-svg-demo>
        </div>
      `
    };
  });
