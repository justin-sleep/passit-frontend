import { Injectable, ErrorHandler, Injector } from "@angular/core";
import * as Sentry from "@sentry/browser";
import { Store, select } from "@ngrx/store";
import { getOptInErrorReporting } from "./app.reducers";

@Injectable()
export class RavenErrorHandler implements ErrorHandler {
  reportError = false;

  /** Can't use normal dependency injection
   * https://stackoverflow.com/a/41585902/443457
   */
  constructor(injector: Injector) {
    setTimeout(() => {
      const store = injector.get(Store);
      store
        .pipe(select(getOptInErrorReporting))
        .subscribe(optIn => (this.reportError = optIn));
    });
  }

  handleError(error: any) {
    if (this.reportError) {
      Sentry.captureException(error.originalError || error);
    } else {
      console.warn("Did not send error report because user did not opt in.");
    }
    console.error(error);
  }
}
