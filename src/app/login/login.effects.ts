import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { Store, select } from "@ngrx/store";
import {
  filter,
  switchMap,
  map,
  distinctUntilChanged,
  catchError,
  tap,
  withLatestFrom,
  exhaustMap
} from "rxjs/operators";
import {
  StartAsyncValidationAction,
  ClearAsyncErrorAction,
  SetAsyncErrorAction
} from "ngrx-forms";
import { concat, timer, of } from "rxjs";

import { UserService } from "../account/user";
import { IState } from "../app.reducers";
import { getLoginForm } from "./login.reducer";
import {
  LoginAction,
  AppActionTypes,
  LoginSuccessAction,
  LoginFailureAction
} from "../app.actions";
import * as SetPasswordActions from "../account/reset-password/set-password/set-password.actions";
import { IS_EXTENSION } from "../constants";

@Injectable()
export class LoginEffects {
  /** Implements "as you type" check to determine if the server url is valid or not */
  @Effect()
  asyncServerUrlCheck$ = this.store.pipe(select(getLoginForm)).pipe(
    filter(form => form.value.showUrl),
    distinctUntilChanged(
      (first, second) => first.value.url === second.value.url
    ),
    switchMap(form =>
      concat(
        timer(300).pipe(
          map(
            () => new StartAsyncValidationAction(form.controls.url.id, "exists")
          )
        ),
        this.userService.checkUrl(form.value.url).pipe(
          map(() => new ClearAsyncErrorAction(form.controls.url.id, "exists")),
          catchError(() => [
            new SetAsyncErrorAction(
              form.controls.url.id,
              "exists",
              form.value.url
            )
          ])
        )
      )
    )
  );

  @Effect()
  login$ = this.actions$.pipe(
    ofType<LoginAction>(AppActionTypes.LOGIN),
    withLatestFrom(this.store.pipe(select(getLoginForm))),
    map(([action, form]) => form.value),
    exhaustMap(auth => {
      const callLogin = () => {
        return this.userService
          .login(
            auth.email,
            auth.password,
            auth.rememberMe ? auth.rememberMe : false
          )
          .pipe(
            map(resp => new LoginSuccessAction(resp)),
            catchError(err => of(new LoginFailureAction(err)))
          );
      };
      const callCheckAndSetUrl = (url: string) => {
        return this.userService.checkAndSetUrl(url).pipe(
          exhaustMap(() => callLogin()),
          catchError(err => of(new LoginFailureAction(err)))
        );
      };

      if (auth.url) {
        return callCheckAndSetUrl(auth.url);
      } else {
        return callLogin();
      }
    })
  );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType<LoginSuccessAction>(
      AppActionTypes.LOGIN_SUCCESS,
      SetPasswordActions.setPasswordSuccess.type
    ),
    tap(action => {
      if (IS_EXTENSION) {
        this.router.navigate(["/popup"]);
      } else {
        if (action.type === AppActionTypes.LOGIN_SUCCESS) {
          if (action.payload.mfaRequired) {
            this.router.navigate(["/account/login/verify-mfa"]);
            return;
          }
        }
        this.router.navigate(["/list"]);
      }
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private userService: UserService,
    private router: Router
  ) {}
}
