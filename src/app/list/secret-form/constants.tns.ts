import { DeviceType } from "tns-core-modules/ui/enums";
import { device } from "tns-core-modules/platform";

export const IS_TABLET = device.deviceType === DeviceType.Tablet;
