import {
  createFormGroupState,
  FormGroupState,
  validate,
  updateGroup,
  createFormStateReducerWithUpdate
} from "ngrx-forms";
import {
  SecretFormActionTypes,
  SecretFormActionsUnion
} from "./secret-form.actions";
import {
  SecretActionTypes,
  SecretActionsUnion
} from "../../secrets/secret.actions";
import { required } from "ngrx-forms/validation";
import { ListActionTypes, ListActionsUnion } from "../list.actions";

export interface ISecretForm {
  name: string;
  searchGroups: string;
  username: string;
  url: string;
  password: string;
  notes: string;
}

export const SECRET_FORM_ID = "Secret Form";
export const validateAndUpdateFormState = updateGroup<ISecretForm>({
  name: validate(required)
});
export const initialFormState = validateAndUpdateFormState(
  createFormGroupState<ISecretForm>(SECRET_FORM_ID, {
    name: "",
    searchGroups: "",
    username: "",
    url: "",
    password: "",
    notes: ""
  })
);

export const setFormValues = (values: ISecretForm) =>
  createFormGroupState<ISecretForm>(SECRET_FORM_ID, values);

export interface ISecretFormState {
  secretId: number | null;
  form: FormGroupState<ISecretForm>;
  selectedGroups: number[];
  errorMessage: string | null;
  isUpdating: boolean;
  isUpdated: boolean;
  showNotes: boolean;
  passwordIsMasked: boolean;
}

export const initialState: ISecretFormState = {
  secretId: null,
  form: initialFormState,
  selectedGroups: [],
  errorMessage: null,
  isUpdated: false,
  isUpdating: false,
  showNotes: false,
  passwordIsMasked: true
};

export const formReducer = createFormStateReducerWithUpdate<ISecretForm>(
  validateAndUpdateFormState
);

export function reducer(
  state = initialState,
  action: SecretFormActionsUnion | SecretActionsUnion | ListActionsUnion
): ISecretFormState {
  const form = formReducer(state.form, action);
  if (form !== state.form) {
    state = { ...state, form };
  }

  switch (action.type) {
    case SecretFormActionTypes.SET_FORM_DATA:
      let showNotes = false;
      if (action.payload.formData.notes) {
        showNotes = true;
      }
      return {
        ...initialState,
        secretId: action.payload.formData.id,
        form: setFormValues(action.payload.formData),
        selectedGroups: action.payload.groupIds,
        showNotes
      };

    case SecretFormActionTypes.TOGGLE_PASSWORD_IS_MASKED:
      return {
        ...state,
        passwordIsMasked: !state.passwordIsMasked
      };

    case SecretFormActionTypes.SET_PASSWORD_IS_MASKED:
      return {
        ...state,
        passwordIsMasked: action.payload
      };

    case SecretFormActionTypes.TOGGLE_SHOW_NOTES:
      return {
        ...state,
        showNotes: !state.showNotes
      };

    case SecretFormActionTypes.CREATE_SECRET:
    case SecretFormActionTypes.UPDATE_SECRET:
      return {
        ...state,
        isUpdating: true,
        isUpdated: false
      };

    case SecretFormActionTypes.CREATE_SECRET_SUCCESS:
      return {
        ...state,
        isUpdating: false,
        isUpdated: true,
        errorMessage: initialState.errorMessage
      };

    case SecretFormActionTypes.CREATE_SECRET_FAILURE:
      return {
        ...state,
        errorMessage: action.payload
      };

    case SecretActionTypes.REPLACE_SECRET:
      return {
        ...state,
        isUpdating: true,
        isUpdated: false
      };

    case SecretFormActionTypes.DECRYPT_ERROR:
      return {
        ...state,
        form: setFormValues(action.payload.form),
        errorMessage: action.payload.message
      };

    case SecretFormActionTypes.ADD_GROUP_TO_SECRET:
      return {
        ...state,
        selectedGroups: [...state.selectedGroups].concat(action.payload)
      };

    case SecretFormActionTypes.REMOVE_GROUP_FROM_SECRET:
      return {
        ...state,
        selectedGroups: [...state.selectedGroups].filter(
          groupId => groupId !== action.payload
        )
      };

    case SecretActionTypes.REPLACE_SECRET_SUCCESS:
      return {
        ...state,
        isUpdating: false,
        isUpdated: true,
        errorMessage: initialState.errorMessage
      };

    case ListActionTypes.SHOW_CREATE:
      return {
        ...initialState,
        form: formReducer(initialState.form, action)
      };
  }
  return state;
}

export const getForm = (state: ISecretFormState) => state.form;
export const getErrorMessage = (state: ISecretFormState) => state.errorMessage;
export const getSecretId = (state: ISecretFormState) => state.secretId;
export const getSecretIsNew = (state: ISecretFormState) =>
  state.secretId === null;
export const getIsUpdating = (state: ISecretFormState) => state.isUpdating;
export const getIsUpdated = (state: ISecretFormState) => state.isUpdated;
export const getPasswordIsMasked = (state: ISecretFormState) =>
  state.passwordIsMasked;
export const getShowNotes = (state: ISecretFormState) => state.showNotes;
export const getSelectedGroups = (state: ISecretFormState) =>
  state.selectedGroups;
