import { Routes } from "@angular/router";
import { LoginContainer } from "./login/login.container";
import { AlreadyLoggedInGuard, LoggedInGuard } from "./guards";
import { SecretListContainer } from "./list";
import { VerifyMfaContainer } from "./login/verify-mfa/verify-mfa.container";

export const routes: Routes = [
  { path: "", redirectTo: "list", pathMatch: "full" },
  {
    path: "account/login",
    component: LoginContainer,
    canActivate: [AlreadyLoggedInGuard],
    data: {
      title: "Log In",
      showNavBar: false
    }
  },
  {
    path: "account/login/verify-mfa",
    component: VerifyMfaContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Verify MFA",
      showNavBar: false
    }
  },
  {
    path: "list",
    component: SecretListContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Password List"
    }
  },
  {
    path: "groups",
    canActivate: [LoggedInGuard],
    loadChildren: () =>
      import("./groups/groups.module").then(mod => mod.GroupsModule)
  },
  {
    path: "account",
    loadChildren: "./account/account.module#AccountModule"
  },
  {
    path: "import",
    loadChildren: "./importer/importer.module#ImporterModule"
  },
  {
    path: "export",
    loadChildren: "./exporter/exporter.module#ExporterModule"
  }
];
