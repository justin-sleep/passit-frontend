import { Routes } from "@angular/router";
import { ImporterContainer } from "./importer.container";
import { LoggedInGuard } from "../guards";

export const componentDeclarations: any[] = [];

export const providerDeclarations: any[] = [];

export const routes: Routes = [
  {
    path: "",
    component: ImporterContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Import Passwords"
    }
  }
];
