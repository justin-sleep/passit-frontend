import { AccountActions, AccountActionTypes } from "./account/account.actions";
import {
  RegisterActionTypes,
  RegisterAction,
  RegisterSuccessAction
} from "./account/register/register.actions";
import {
  ErrorReportingTypes,
  ErrorReportingActionsUnion
} from "./account/error-reporting/error-reporting.actions";
import * as ResetPasswordVerifyActions from "./account/reset-password/reset-password-verify/reset-password-verify.actions";
import * as SetPasswordActions from "./account/reset-password/set-password/set-password.actions";
import {
  ChangePasswordSubmitFormSuccess,
  ChangePasswordActionTypes
} from "./account/change-password/change-password.actions";
import { AppActions, AppActionTypes } from "./app.actions";
import {
  ManageMfaActions,
  ManageMfaActionTypes
} from "./account/manage-mfa/manage-mfa.actions";

export interface IAuthState {
  email: string | null;
  userId: number | null;
  url: string | null;
  userToken: string | null;
  privateKey: string | null;
  publicKey: string | null;
  rememberMe: boolean;
  optInErrorReporting: boolean;
  mfaRequired: boolean;
  /** Always redirect logged in user to set password page */
  forceSetPassword: boolean;
}

export const initialState: IAuthState = {
  email: null,
  userId: null,
  url: null,
  userToken: null,
  privateKey: null,
  publicKey: null,
  rememberMe: false,
  optInErrorReporting: false,
  mfaRequired: false,
  forceSetPassword: false
};

export function authReducer(
  state = initialState,
  action:
    | AccountActions
    | AppActions
    | RegisterAction
    | RegisterSuccessAction
    | ErrorReportingActionsUnion
    | any // Due to action creator usage
    | ChangePasswordSubmitFormSuccess
    | ManageMfaActions
): IAuthState {
  switch (action.type) {
    case AppActionTypes.LOGIN:
    case RegisterActionTypes.REGISTER:
      // Clear any stale state (except the url)
      return { ...initialState, url: state.url };

    case AppActionTypes.LOGIN_SUCCESS:
    case RegisterActionTypes.REGISTER_SUCCESS:
      return {
        ...state,
        email: action.payload.email,
        userId: action.payload.userId,
        userToken: action.payload.userToken,
        privateKey: action.payload.privateKey,
        publicKey: action.payload.publicKey,
        rememberMe: action.payload.rememberMe,
        optInErrorReporting: action.payload.optInErrorReporting,
        mfaRequired: action.payload.mfaRequired
      };

    case ResetPasswordVerifyActions.verifyAndLoginSuccess.type:
      return {
        ...state,
        email: action.auth.email,
        userId: action.auth.userId,
        userToken: action.auth.userToken,
        privateKey: action.auth.privateKey,
        publicKey: action.auth.publicKey,
        rememberMe: action.auth.rememberMe,
        optInErrorReporting: action.auth.optInErrorReporting,
        mfaRequired: action.auth.mfaRequired
      };

    case ChangePasswordActionTypes.SUBMIT_FORM_SUCCESS:
      return {
        ...state,
        userToken: action.payload.token,
        privateKey: action.payload.privateKey,
        publicKey: action.payload.publicKey
      };

    case AccountActionTypes.SET_URL:
      return {
        ...state,
        url: action.payload
      };

    case SetPasswordActions.setPasswordSuccess.type:
      return {
        ...state,
        forceSetPassword: false
      };

    case ErrorReportingTypes.SAVE_FORM_SUCCESS:
      return {
        ...state,
        optInErrorReporting: action.payload.opt_in_error_reporting
      };

    case SetPasswordActions.forceSetPassword.type:
      return {
        ...state,
        forceSetPassword: true
      };

    case ManageMfaActionTypes.ACTIVATE_MFA_SUCCESS:
      return {
        ...state,
        mfaRequired: true
      };

    case ManageMfaActionTypes.DEACTIVATE_MFA_SUCCESS:
      return {
        ...state,
        mfaRequired: false
      };

    default:
      return state;
  }
}
