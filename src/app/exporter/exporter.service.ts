import { Injectable } from "@angular/core";
import { saveAs } from "file-saver";
import { NgPassitSDK } from "../ngsdk/sdk";
import { ExporterServiceBase } from "./exporter.service.common";

@Injectable()
export class ExporterService extends ExporterServiceBase {
  HEADERS = ["name", "username", "url", "password", "extra"];

  constructor(sdk: NgPassitSDK) {
    super(sdk);
  }

  async exportSecrets(): Promise<void> {
    const secrets = await this.getSecrets();
    const csv = this.serialize(secrets);
    const blob = new Blob([csv], { type: "text/csv" });
    saveAs(blob, "secrets.csv");
  }
}
