import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { Store, select } from "@ngrx/store";
import { exhaustMap, map, catchError, withLatestFrom } from "rxjs/operators";
import { EMPTY } from "rxjs";
import { GroupService } from "../groups/group.service";
import { acceptInvite, declineInvite } from "./groups.actions";
import { IAppState } from "./groups.reducer";
import { loadGroups } from "../data/groups.actions";
import { getUserId } from "../app.reducers";

@Injectable()
export class GroupsModuleEffects {
  acceptInvite$ = createEffect(() =>
    this.actions$.pipe(
      ofType(acceptInvite),
      withLatestFrom(this.store.pipe(select(getUserId))),
      exhaustMap(([action, userId]) => {
        if (userId) {
          return this.groupService.acceptGroup(action.group, userId).pipe(
            map(() => loadGroups()),
            catchError(() => EMPTY)
          );
        }
        return EMPTY;
      })
    )
  );

  declineInvite$ = createEffect(() =>
    this.actions$.pipe(
      ofType(declineInvite),
      withLatestFrom(this.store.pipe(select(getUserId))),
      exhaustMap(([action, userId]) => {
        if (userId) {
          return this.groupService.declineGroup(action.group, userId).pipe(
            map(() => loadGroups()),
            catchError(() => EMPTY)
          );
        }
        return EMPTY;
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<IAppState>,
    private groupService: GroupService
  ) {}
}
