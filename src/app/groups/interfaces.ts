import { IContact } from "../data/interfaces";

export interface IGroupContact extends IContact {
  isSelected: boolean;
  isPending: boolean;
}
