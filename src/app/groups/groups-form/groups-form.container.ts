import { Component, ChangeDetectionStrategy } from "@angular/core";
import { Store, select } from "@ngrx/store";
import {
  selectGroupsForm,
  IAppState,
  selectGroupsPageIsShowingNewForm,
  selectGroupContacts,
  selectGroupFormIsUpdating,
  selectGroupFormIsUpdated
} from "../groups.reducer";
import { hideCreate } from "../groups.actions";
import {
  saveGroup,
  deleteGroup,
  addUserToGroup,
  removeUserFromGroup
} from "./groups-form.actions";
import { getUserId, getIsPrivateOrgMode } from "../../app.reducers";

@Component({
  selector: "groups-form-container",
  template: `
    <groups-form
      [form]="form$ | async"
      [contacts]="contacts$ | async"
      [isNew]="isNew$ | async"
      [isUpdating]="isUpdating$ | async"
      [isUpdated]="isUpdated$ | async"
      [isPrivateOrgMode]="isPrivateOrgMode$ | async"
      [userId]="userId$ | async"
      [keyControls]="_keyControls"
      [searchFieldValidationPattern]="emailValidationPattern"
      [invalidSearchFieldText]="invalidSearchFieldText"
      [noSearchFieldMatchText]="noSearchFieldMatchText"
      [noListItemsText]="_noListItemsText"
      [fewListItemsText]="_fewListItemsText"
      [allListItemsSelectedText]="_allListItemsSelectedText"
      [justAddedText]="_justAddedText"
      (toggleKeyControls)="toggleKeyControls()"
      (cancelForm)="cancelForm()"
      (submitForm)="saveForm()"
      (delete)="delete()"
      (addUserToGroup)="addUserToGroup($event)"
      (removeUserFromGroup)="removeUserFromGroup($event)"
    ></groups-form>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupsFormContainer {
  isNew$ = this.store.pipe(select(selectGroupsPageIsShowingNewForm));
  form$ = this.store.pipe(select(selectGroupsForm));
  contacts$ = this.store.pipe(select(selectGroupContacts));
  isUpdating$ = this.store.pipe(select(selectGroupFormIsUpdating));
  isUpdated$ = this.store.pipe(select(selectGroupFormIsUpdated));
  userId$ = this.store.pipe(select(getUserId));
  isPrivateOrgMode$ = this.store.pipe(select(getIsPrivateOrgMode));
  _keyControls = false;
  _isPrivateOrgMode: boolean;

  // Multiselect variables
  emailValidationPattern = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
  invalidSearchFieldText = "Enter a valid email address.";
  noSearchFieldMatchText = "No Passit user with this email address.";
  _noListItemsText =
    "Enter a Passit user's complete email address above to invite that person to a group.";
  _fewListItemsText =
    "This list shows Passit users that you have added to your other groups.";
  _allListItemsSelectedText = "";
  _justAddedText = `
    Once you press “Save Group”, ||| will be invited. When people you invite
    accept your invitation, their email addresses will be visible here when you
    create or edit another group.
  `;

  constructor(private store: Store<IAppState>) {
    this.isPrivateOrgMode$.subscribe(isPrivateOrgMode => {
      if (isPrivateOrgMode) {
        this._isPrivateOrgMode = isPrivateOrgMode;
        this._justAddedText =
          "Once you press “Save Group”, ||| will be invited.";
        this._noListItemsText =
          "Looks like you're all alone! You can still create groups as a way to organize your passwords.";
        this._allListItemsSelectedText = "Everyone is in the group!";
        this._fewListItemsText =
          "This list shows other Passit users that you can add to your groups.";
      }
    });
  }

  cancelForm() {
    this.store.dispatch(hideCreate());
  }

  saveForm() {
    /**
     * If we're in the multiselect add widget, we don't want the Enter key to
     * submit this
     */
    if (!this._keyControls) {
      this.store.dispatch(saveGroup());
    }
  }

  delete() {
    this.store.dispatch(deleteGroup());
  }

  addUserToGroup(userId: number) {
    this.store.dispatch(
      addUserToGroup({ userId, isPrivateOrgMode: this._isPrivateOrgMode })
    );
  }

  removeUserFromGroup(userId: number) {
    this.store.dispatch(removeUserFromGroup({ userId }));
  }

  toggleKeyControls = () => (this._keyControls = !this._keyControls);
}
