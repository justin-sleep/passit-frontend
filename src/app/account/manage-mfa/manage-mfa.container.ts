import { Component } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { IState, getMfaRequired } from "../../app.reducers";
import {
  EnableMfa,
  ForwardStep,
  ActivateMfa,
  DeactivateMfa
} from "./manage-mfa.actions";
import {
  getMfaProvisioningURI,
  getMfaStep,
  getEnableMfaForm,
  getMfaErrorMessage
} from "../account.reducer";

@Component({
  template: `
    <app-manage-mfa
      [uri]="uri$ | async"
      [step]="step$ | async"
      [form]="form$ | async"
      [errors]="errors$ | async"
      [mfaRequired]="mfaRequired$ | async"
      (forwardStep)="forwardStep()"
      (generateMfa)="generateMfa()"
      (verifyMfa)="activateMfa()"
      (deactivateMfa)="deactivateMfa()"
    ></app-manage-mfa>
  `
})
export class ManageMfaContainer {
  uri$ = this.store.pipe(select(getMfaProvisioningURI));
  step$ = this.store.pipe(select(getMfaStep));
  form$ = this.store.pipe(select(getEnableMfaForm));
  errors$ = this.store.pipe(select(getMfaErrorMessage));
  mfaRequired$ = this.store.pipe(select(getMfaRequired));
  constructor(private store: Store<IState>) {}

  forwardStep() {
    this.store.dispatch(new ForwardStep());
  }

  generateMfa() {
    this.store.dispatch(new EnableMfa());
  }

  activateMfa() {
    this.store.dispatch(new ActivateMfa());
  }

  deactivateMfa() {
    this.store.dispatch(new DeactivateMfa());
  }
}
