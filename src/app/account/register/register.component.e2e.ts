// describe("Register", () => {
//
//   beforeEach(() => {
//     // change hash depending on router LocationStrategy
//     browser.get("/register");
//   });
//
//   it("should have a title", () => {
//     let subject = browser.getTitle();
//     let result  = "Passit";
//     expect(subject).toEqual(result);
//   });
//
//   it("forms should have correct labels", () => {
//     let emailEl = $("#userEmailLabel");
//     let passwordEl = $("#userPwLabel");
//
//     let emailResult = "Email:";
//     let pWResult = "Password:";
//
//     emailEl.isDisplayed().then(() => {
//       emailEl.getText().then((text) => {
//         expect(emailResult).toEqual(text);
//       });
//
//       passwordEl.getText().then((pwText) => {
//         expect(pWResult).toEqual(pwText);
//       });
//     });
//   });
//
//   it("user should be able to register", () => {
//     let loginBtn = element(by.id("btn-login"));
//
//     let emailEl = $("#userEmail");
//     let passwordEl = $("#userPw");
//
//     let rgEm = Math.floor((Math.random() * 1000) + 10).toString() + "@gmail.com";
//
//     emailEl.sendKeys(rgEm);
//     passwordEl.sendKeys("test");
//
//     loginBtn.click();
//
//     browser.wait(() => {
//       return element(by.id("login-form")).isPresent();
//     }, 6000);
//
//     expect(element(by.id("login-form"))).toBeDefined();
//
//   });
//
//   it("should redirect to login on button click", () => {
//     let returnBtn = element(by.id("btn-signup"));
//
//     returnBtn.click();
//
//     browser.wait(() => {
//       return element(by.id("login-form")).isPresent();
//     }, 6000);
//
//     expect(element(by.id("login-form"))).toBeDefined();
//   });
//
// });
