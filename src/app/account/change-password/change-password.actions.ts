import { Action } from "@ngrx/store";

export enum ChangePasswordActionTypes {
  SUBMIT_FORM = "[Change Password] Submit",
  SUBMIT_FORM_SUCCESS = "[Change Password] Submit Success",
  SUBMIT_FORM_FAILURE = "[Change Password] Submit Failure",
  RESET_FORM = "[Change Password] Reset"
}

export class ChangePasswordSubmitForm implements Action {
  readonly type = ChangePasswordActionTypes.SUBMIT_FORM;
}

export class ChangePasswordSubmitFormSuccess implements Action {
  readonly type = ChangePasswordActionTypes.SUBMIT_FORM_SUCCESS;

  constructor(
    public payload: {
      backupCode: string;
      token: string;
      privateKey: string;
      publicKey: string;
    }
  ) {}
}

export class ChangePasswordSubmitFormFailure implements Action {
  readonly type = ChangePasswordActionTypes.SUBMIT_FORM_FAILURE;

  constructor(public payload: string[]) {}
}

export class ResetForm implements Action {
  readonly type = ChangePasswordActionTypes.RESET_FORM;
}

export type ChangePasswordActionsUnion =
  | ChangePasswordSubmitForm
  | ChangePasswordSubmitFormSuccess
  | ChangePasswordSubmitFormFailure
  | ResetForm;
