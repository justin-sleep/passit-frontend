import { Component, Input } from '@angular/core';
import { BackupCodePdfService } from "../../backup-code-pdf.service";

@Component({
  selector: 'app-download-backup-code',
  templateUrl: './download-backup-code.component.html',
  styleUrls: ['./download-backup-code.component.scss', "../manage-backup-code.component.scss"]
})
export class DownloadBackupCodeComponent {

  @Input()
  code: string;
  @Input()
  hasFinished: boolean;

  constructor(private backupCodeToPdf: BackupCodePdfService) {
  }

  downloadPDF() {
    this.backupCodeToPdf.download(this.code);
  }
}
