import {
  Base64Binary,
  Encrypted,
  KeyPair,
  Password,
  PrivateKey,
  PublicKey,
  sodium,
  SymmetricKey,
  UTF8Binary
} from "../simple_asym/asymmetric_encryption";
import { makeRandomTypableString } from "../simple_asym/crypto";
import Api from "./api";
import * as api from "./api.interfaces";

import { INewSecret, ISecret } from "./sdk.interfaces";

import { API_URL } from "./constants";
import { authRequired } from "./decorators";
import { hashPassword, MD5 } from "./hash";

import { slugify } from "./utils";

function assignDefined<T>(target: { [P in keyof T]?: T[P] }, source: T): void {
  for (const key in source) {
    if ((source as any).hasOwnProperty(key)) {
      const val = source[key];
      if (val !== undefined) {
        target[key] = val;
      }
    }
  }
}

function toMap<T>(o: { [propName: string]: T }): Map<string, T> {
  const m = new Map<string, T>();
  for (const key in o) {
    if (o.hasOwnProperty(key)) {
      m.set(key, o[key]);
    }
  }
  return m;
}

function fromMap<T>(m: Map<string, T>): { [propName: string]: T } {
  const o = {};
  m.forEach((v, k) => (o[k] = v));
  return o;
}

function mapKeys<T, V, U>(
  m: Map<T, V>,
  fn: (this: void, v: V) => U
): Map<T, U> {
  function transformPair([k, v]: [T, V]): [T, U] {
    return [k, fn(v)];
  }
  return new Map(Array.from(m.entries(), transformPair));
}

/** encode a string as base64 */
function encodeAuthBase64(s: string): string {
  return new Base64Binary(new UTF8Binary(s).binary).string;
}

// all of these legacy functions are to preserve encoding weirdness's we had unintentionally
// and now must keep so that old data works. New data doesn't need this however.

function decodeSymKeyLegacy(s: SymmetricKey): SymmetricKey {
  // Legacy symmetric keys are  always 44 in length
  if (s.binary.length === 44) {
    return new SymmetricKey(new UTF8Binary(s.binary).string);
  }
  return s;
}

function decodePrivateKeyLegacy(p: PrivateKey): PrivateKey {
  // Legacy symmetric keys are  always 44 in length
  if (p.binary.length === 44) {
    return new PrivateKey(new UTF8Binary(p.binary).string);
  }
  return p;
}

export default class PassitSDK {
  userId: number;
  api: Api;
  keyPair: KeyPair;

  constructor() {
    this.api = new Api(API_URL);
  }

  /**
   * Set internal state including RSA keys and authentication
   * @returns Promise to indicate set up is complete.
   */
  public async set_up(
    publicKey: string,
    privateKey: string,
    userId: number,
    authToken: string
  ) {
    await sodium.ready;
    this.userId = userId;
    this.api.token = authToken;
    this.set_keys(publicKey, privateKey);
  }

  /** Set the base url for API calls */
  public set_url(url: string) {
    this.api.baseUrl = url;
  }

  /** Ensure sdk is ready to run
   * Only needs checked if wanting to check explicitly
   * for example if set_up function was not run.
   */
  public async ready() {
    return await sodium.ready;
  }

  public set_keys(publicKey: string, privateKey: string) {
    this.keyPair = new KeyPair(
      new PublicKey(publicKey),
      new PrivateKey(privateKey)
    );
  }

  public get_conf(): Promise<api.IConf> {
    return this.api.getConf();
  }

  /**
   * Check if a email is avaible for signup
   *
   * @param {string} email - the email to check
   * @return {Promise<boolean>} true if the username is available
   *
   * Example:
   * ```
   * let sdk = new PassitSDK();
   * sdk.is_username_available("superkool37@example.com"); // true
   * ```
   */
  public async is_username_available(email: string): Promise<boolean> {
    const { available } = await this.api.usernameAvailable(email);
    return available;
  }

  /**
   * Sign up for a new user account - log in as well.
   * The generated backup code will not saved anywhere, so it needs to be
   * displayed to the user before being discarded.
   * @param email - the email used for signup
   * @param password - the users password
   * @param first_name - the users first name
   * @param last_name - the users last name
   * @return generated user, auth token, backup code, and private key
   *
   * Example:
   * ```
   * let sdk = new PassitSDK();
   * sdk.sign_up("superkool37@example.com", "hunter2")
   * ```
   */
  public async sign_up(
    email: string,
    password: string,
    firstName = "",
    lastName = ""
  ) {
    await sodium.ready;
    const hashObj = hashPassword(password);
    this.keyPair = KeyPair.generate();
    const { publicKey, privateKey } = this.keyPair;
    const backupCode = makeRandomTypableString();
    const privateKeyBackup = new Password(backupCode).encrypt(privateKey)
      .string;
    await this.api.createUser({
      client_salt: hashObj.salt,
      email,
      password: hashObj.hash,
      private_key: new Password(password).encrypt(privateKey).string,
      public_key: publicKey.string,
      private_key_backup: privateKeyBackup
    });
    const res = await this.do_log_in(email, hashObj.hash, password);
    return {
      ...res,
      backupCode
    };
  }

  /**
   * Log the user in
   * @param email - the users email
   * @param password - the users password
   * @param expires - Optional param to set when the token expires (in hours)
   * @return User response, auth token, and private key as base64
   *
   * Example:
   * ```
   * let sdk = new PassitSDK();
   * sdk.sign_up("superkool37@example.com", "hunter2").then(user => {
   *   sdk.log_in("superkool37@example.com", "hunder2");
   * });
   * ```
   */
  public async log_in(
    email: string,
    password: string,
    expires?: number
  ): Promise<{ user: api.IUser; token: string; privateKey: string }> {
    await sodium.ready;
    const { client_salt } = await this.api.getPublicAuth(email);
    const { hash } = hashPassword(password, client_salt);
    return this.do_log_in(email, hash, password, expires);
  }

  public logout() {
    return this.api.logout();
  }

  public logoutAll() {
    return this.api.logoutAll();
  }

  /** Update a user */
  @authRequired
  public update_user(id: number, user: Partial<api.ICreateUser>) {
    return this.api.updateUser(id, user);
  }

  /**
   * Confirm email account using long code from web link
   * @param code long code from web link
   */
  public async confirm_email_long_code(code: string): Promise<number> {
    return (await this.api.confirmEmailLongCode(code)).user_id;
  }

  /**
   * Confirm email account using short, typable code from email.
   * Requires authetnication
   * @param code short code from email
   */
  public async confirm_email_short_code(code: string): Promise<number> {
    return (await this.api.confirmEmailShortCode(code)).user_id;
  }

  /**
   * Request a new email confirmation code
   */
  public async request_new_confirmation(): Promise<void> {
    return await this.api.requestNewConfirmation();
  }

  public async lookup_user_id(email: string): Promise<number> {
    return (await this.api.lookupUserID(email)).id;
  }

  /**
   * Delete own user account.
   * @param password - plaintext password
   */
  public async deleteOwnAccount(password: string) {
    const user = await this.api.getUser();
    const hash = hashPassword(password, user.client_salt).hash;
    return this.api.deleteUser(this.userId, hash);
  }

  /**
   * Create a new group
   * @param name - the new group name
   * @param slug - the new group slug
   * @return a promise with the created group object
   *
   * Example:
   * ```
   * sdk.create_group("Cool People").then(group => {
   *   group.slug; // cool-people
   * });
   */
  @authRequired
  public create_group(
    name: string,
    slug?: string
  ): Promise<api.ICreateGroupResp> {
    if (slug == null) {
      slug = slugify(name);
    }
    const { privateKey, publicKey } = KeyPair.generate();
    const { key_ciphertext, private_key_ciphertext } = this.generateGroupKey(
      privateKey,
      this.keyPair.publicKey
    );

    return this.api.createGroup({
      key_ciphertext: key_ciphertext.string,
      name,
      private_key_ciphertext: private_key_ciphertext.string,
      public_key: publicKey.string,
      slug
    });
  }

  /** Update existing group using PATCH */
  @authRequired
  public update_group({
    id,
    name,
    slug
  }: {
    id: number;
    name: string;
    slug: string;
  }): Promise<api.ICreateGroupResp> {
    return this.api.updateGroup(id, { name, slug });
  }

  /**
   * Delete a group using its id
   * @param groupId - the id of secret to get
   * @returns a 204 if successful
   */
  @authRequired
  public delete_group(groupId: number): Promise<void> {
    return this.api.deleteGroup(groupId);
  }

  /**
   * Get a group object with a group_id
   * @param groupID - the id of the group to get
   * @return Group object
   */
  @authRequired
  public get_group(groupID: number): Promise<api.IGroup> {
    return this.api.getGroup(groupID);
  }

  @authRequired
  public acceptGroupInvite(groupUserId: number) {
    return this.api.acceptGroupInvite(groupUserId);
  }

  @authRequired
  public declineGroupInvite(groupUserId: number) {
    return this.api.declineGroupInvite(groupUserId);
  }

  @authRequired
  public list_groups(): Promise<api.IGroup[]> {
    return this.api.getGroups();
  }

  /**
   * Add a new user to an existing group
   * @param groupID - the id of the group to add to user to
   * @param userID - the id of the user to add
   * @return a promise with the response of the action
   */
  @authRequired
  public async add_user_to_group(
    groupID: number,
    userID: number,
    email: string
  ): Promise<api.IGroupUser> {
    const { privateKey } = this._getGroupKeyPair(
      await this.api.getGroup(groupID)
    );
    const userPubKey = new PublicKey(
      (await this.api.getUserPublicKey(userID)).public_key
    );

    const { key_ciphertext, private_key_ciphertext } = this.generateGroupKey(
      privateKey,
      userPubKey
    );
    return this.api.createGroupUser(groupID, {
      key_ciphertext: key_ciphertext.string,
      private_key_ciphertext: private_key_ciphertext.string,
      user: userID,
      user_email: email
    });
  }

  @authRequired
  public remove_user_from_group(
    groupId: number,
    groupUserId: number
  ): Promise<void> {
    return this.api.deleteGroupUser(groupId, groupUserId);
  }

  /** Get all contacts */
  @authRequired
  public list_contacts(): Promise<api.IContact[]> {
    return this.api.getContacts();
  }

  /**
   * Send secrets to a server
   * @param secret - A new secret object
   * @param secret.name - the name of the secrets
   * @param secret.type - the type  of the secrets
   * @param secret.visible_data - the visible info about the secrets
   * @param secret.secrets - the secrets to be encrypted
   * @param secret.group_id - the group id to use the pub key from if any
   * @return a promise with the response
   *
   * Example:
   * ```
   * let secretData = {
   *   "name": "passit.io",
   *   "type": "website",
   *   "visible_data": {"username": "foobaruser"},
   *   "secrets": {"password": "aE3fDejfow#d"},
   * }
   * sdk.create_secret(secretData).then(secret => {
   *   secret.id; // 1
   * });
   */
  @authRequired
  public async create_secret(secret: INewSecret): Promise<api.ISecret> {
    const [encryptedKey, encrSecrets] = this._encrypt_secrets(
      mapKeys(toMap(secret.secrets || {}), (s: any) => new UTF8Binary(s)),
      secret.group_id
        ? new PublicKey((await this.get_group(secret.group_id)).public_key)
        : undefined
    );
    return this.api.createSecret({
      data: secret.visible_data || {},
      name: secret.name || "",
      secret_through_set: [
        {
          data: fromMap(mapKeys(encrSecrets, s => s.string)),
          group: secret.group_id,
          key_ciphertext: encryptedKey.string
        }
      ],
      type: secret.type
    });
  }

  /**
   * Update a secret
   * @param secret - Secret object to update
   * @param secret.id - the id of the secret to update
   * @param secret.name - the new name of the secret
   * @param secret.visible_data - the visible data of the secret in key/value format
   * @param secret.secrets - the secrets to be encrypted in key/value format
   * @param secret.group_id
   * @return the server response ISecret object
   */
  @authRequired
  public async update_secret(secret: ISecret): Promise<api.ISecret> {
    const patchSecret = {
      name: secret.name,
      type: secret.type,
      data: secret.visible_data
    };
    if (!secret.secrets) {
      return await this.api.updateSecret(secret.id, patchSecret);
    }

    const dbSecret = await this.get_secret(secret.id);
    assignDefined(dbSecret, patchSecret);
    // re-encrypt all secrets
    for (const secretThrough of dbSecret.secret_through_set) {
      const [encryptedKey, encrSecrets] = this._encrypt_secrets(
        mapKeys(toMap(secret.secrets), (s: any) => new UTF8Binary(s)),
        new PublicKey(secretThrough.public_key)
      );
      secretThrough.data = fromMap(mapKeys(encrSecrets, s => s.string));
      secretThrough.key_ciphertext = encryptedKey.string;
    }
    return await this.api.updateSecretWithThroughs(secret.id, dbSecret);
  }

  /**
   * Delete a secret using its id
   * @param secretID - the id of secret to get
   * @returns a 204 if passed
   */
  @authRequired
  public delete_secret(secretID: number): Promise<void> {
    return this.api.deleteSecret(secretID);
  }

  /**
   * Get a secret by its id
   * @param secretID - the id of the secret to get
   * @return a promise with the secret data
   */
  @authRequired
  public get_secret(secretID: number): Promise<api.ISecret> {
    return this.api.getSecret(secretID);
  }

  /**
   * Get the users list of secrets
   * @return a promise with the list in an object
   */
  @authRequired
  public list_secrets(): Promise<api.ISecret[]> {
    return this.api.getSecrets();
  }

  /**
   * Decrypt a secret object
   * @param uniqueSecret - the secret object returned from the server
   * @return a promise with the decrypted secrets in key/value format
   */
  @authRequired
  public async decrypt_secret(uniqueSecret: api.ISecret): Promise<api.IData> {
    const {
      key_ciphertext,
      data,
      group
    } = uniqueSecret.secret_through_set.find(
      through => through.is_mine === true
    )!;
    const decrypted = this._decrypt_secrets(
      new Encrypted<KeyPair, SymmetricKey>(key_ciphertext),
      mapKeys(
        toMap(data),
        (s: any) => new Encrypted<SymmetricKey, UTF8Binary>(s)
      ),
      group ? this._getGroupKeyPair(await this.api.getGroup(group)) : undefined
    );

    return fromMap(mapKeys(decrypted, s => s.string));
  }

  /**
   * Decrypt a secret without making api calls.
   * See decrypt_secret implementation for getting these values from a secret object.
   * @param keyCiphertext - ciphertext of key for this secret
   * @param data - encrypted secrets
   * @param group - Optional group for this secret
   */
  public offline_decrypt_secret(
    keyCiphertext: string,
    data: api.IData,
    group?: api.IGroup
  ): api.IData {
    const decrypted = this._decrypt_secrets(
      new Encrypted<KeyPair, SymmetricKey>(keyCiphertext),
      mapKeys(
        toMap(data),
        (s: any) => new Encrypted<SymmetricKey, UTF8Binary>(s)
      ),
      group ? this._getGroupKeyPair(group) : undefined
    );

    return fromMap(mapKeys(decrypted, s => s.string));
  }

  /**
   * Add a group to a secret
   * @param groupId - the id of the group
   * @param secrets - the secrets to encrypt in key/value
   * @param unique_secret_id - the id of the secret to add the group to
   * @return a promise with the response of the request
   */
  @authRequired
  public async add_group_to_secret(
    groupId: number,
    secret: number | api.ISecret
  ): Promise<api.ISecretThroughGroup> {
    if (typeof secret === "number") {
      secret = await this.get_secret(secret);
    }
    const { public_key } = await this.get_group(groupId);
    const decryptedSecrets = await this.decrypt_secret(secret);
    const [encryptedKey, encryptedSecrets] = await this._encrypt_secrets(
      mapKeys(toMap(decryptedSecrets), (s: any) => new UTF8Binary(s)),
      new PublicKey(public_key)
    );
    return await this.api.createSecretGroup(secret.id, {
      data: fromMap(mapKeys(encryptedSecrets, s => s.string)),
      group: groupId,
      key_ciphertext: encryptedKey.string
    });
  }

  /**
   * Remove a group from a secret
   * @param secret_id - the id of the secret
   * @param secret_through_id
   * @return a promise with the response of the request
   */
  @authRequired
  public remove_group_from_secret(
    secretId: number,
    secretThroughId: number
  ): Promise<void> {
    return this.api.deleteSecretGroup(secretId, secretThroughId);
  }

  /**
   * Change password. This sdk function acts as one large
   * transaction against the backend. It either all fails or all passes.
   * Complex due to needing to change every private key for this user.
   * Sends hash of each changed secret through - to verify it hasn't
   * changed server side
   *
   * Important! change password does NOT relogin user. This should be done on successful change.
   *
   * @param oldPassword - existing password
   * @param newPassword - new password to use
   * @returns { privateKeyBackup, token }
   */
  @authRequired
  public async change_password(oldPassword: string, newPassword: string) {
    // Hash old password
    const user = await this.api.getUser();
    const oldHash = hashPassword(oldPassword, user.client_salt).hash;

    const newUserKeyPair = KeyPair.generate();

    const secretThroughSet = await this.reencryptSecretThroughSet(
      newUserKeyPair
    );
    const groupUserSet = await this.reencryptGroupUserSet(newUserKeyPair);
    const { hash, salt } = hashPassword(newPassword);
    const backupCode = makeRandomTypableString();

    const privateKey = new Password(newPassword).encrypt(
      newUserKeyPair.privateKey
    ).string;
    const privateKeyBackup = new Password(backupCode).encrypt(
      newUserKeyPair.privateKey
    ).string;

    const response = await this.api.changePassword({
      old_password: oldHash,
      user: {
        client_salt: salt,
        password: hash,
        public_key: newUserKeyPair.publicKey.string,
        private_key: privateKey,
        private_key_backup: privateKeyBackup
      },
      secret_through_set: secretThroughSet,
      group_user_set: groupUserSet
    });

    return {
      backupCode,
      token: response.token,
      privateKey: newUserKeyPair.privateKey.string,
      publicKey: newUserKeyPair.publicKey.string
    };
  }

  /**
   * Encrypt all user secrets with supplied user key pair.
   * This async function makes requests to list_secrets
   * secrets is optional, if not set it will fetch them from the API
   */
  public async reencryptSecretThroughSet(
    userKeyPair: KeyPair,
    secrets?: api.ISecret[]
  ) {
    const secretThroughSet: api.IChangePasswordSecretThrough[] = [];
    if (secrets === undefined) {
      secrets = await this.list_secrets();
    }
    for (const secret of secrets) {
      for (const secretThrough of secret.secret_through_set) {
        if (secretThrough.is_mine && secretThrough.group === null) {
          const unencryptedData = this._decrypt_secrets(
            new Encrypted<KeyPair, SymmetricKey>(secretThrough.key_ciphertext),
            mapKeys(
              toMap(secretThrough.data),
              (s: any) => new Encrypted<SymmetricKey, UTF8Binary>(s)
            )
          );
          const [encryptedKey, encryptedSecrets] = this._encrypt_secrets(
            unencryptedData,
            userKeyPair.publicKey
          );
          secretThroughSet.push({
            hash: MD5(secretThrough.key_ciphertext),
            ...secretThrough,
            data: fromMap(mapKeys(encryptedSecrets, s => s.string)),
            key_ciphertext: encryptedKey.string
          });
        }
      }
    }
    return secretThroughSet;
  }

  /**
   * Encrypt all user groups data with supplied user key pair.
   * This async function makes requests to list_groups
   * groups is optional, if undefined it will fetch them form the api
   */
  async reencryptGroupUserSet(userKeyPair: KeyPair, groups?: api.IGroup[]) {
    const groupUserSet: api.IChangePasswordGroupUser[] = [];
    if (groups === undefined) {
      groups = await this.list_groups();
    }
    for (const group of groups) {
      for (const groupUser of group.groupuser_set) {
        // Find a all groupuser_set objects that this user owns
        if (groupUser.user === this.userId) {
          // Generate a new symmetric key for our own access to the
          // existing group's private key
          const groupKeyPair = await this._getGroupKeyPair(group);
          const newGroupKeys = this.generateGroupKey(
            groupKeyPair.privateKey,
            userKeyPair.publicKey
          );
          groupUserSet.push({
            hash: MD5(group.my_key_ciphertext),
            ...groupUser,
            private_key_ciphertext: newGroupKeys.private_key_ciphertext.string,
            key_ciphertext: newGroupKeys.key_ciphertext.string
          });
        }
      }
    }
    return groupUserSet;
  }

  /** Check if this sdk object has been authenticated */
  _check_if_authenticated(): boolean {
    if (this.userId) {
      return true;
    }
    return false;
  }

  /**
   * Decrypt a secret
   * @param encryptedKey - the encrypted aes key used to decrypt the secrets
   * @param encryptedSecret - the secret to decrypt or the secrets to decrypt in an object
   * @param groupPrivateKey - the group private key as a pem string
   * @return the single decrypted secret or the values decrypted in an object
   */
  private _decrypt_secrets(
    encryptedKey: Encrypted<KeyPair, SymmetricKey>,
    encryptedSecret: Map<string, Encrypted<SymmetricKey, UTF8Binary>>,
    keyPair?: KeyPair
  ): Map<string, UTF8Binary> {
    if (!keyPair) {
      keyPair = this.keyPair;
    }
    const key = decodeSymKeyLegacy(keyPair.decrypt(encryptedKey, SymmetricKey));

    return mapKeys(encryptedSecret, s => key.decrypt(s, UTF8Binary));
  }

  /**
   * Get a groups private key
   * @param group - the id of the group we want the key for
   * @return {Promise<[public key, private key]>} a promise with the private key as a pem string
   */
  private _getGroupKeyPair(group: api.IGroup): KeyPair {
    const encrSymKey = new Encrypted<KeyPair, SymmetricKey>(
      group.my_key_ciphertext
    );

    const symKey = decodeSymKeyLegacy(
      this.keyPair.decrypt(encrSymKey, SymmetricKey)
    );
    const encrPrivKey = new Encrypted<SymmetricKey, PrivateKey>(
      group.my_private_key_ciphertext
    );

    const privateKey = decodePrivateKeyLegacy(
      symKey.decrypt(encrPrivKey, PrivateKey)
    );
    return new KeyPair(new PublicKey(group.public_key), privateKey);
  }

  private generateGroupKey(
    groupPrivateKey: PrivateKey,
    userPublicKey: PublicKey
  ): {
    key_ciphertext: Encrypted<KeyPair, SymmetricKey>;
    private_key_ciphertext: Encrypted<SymmetricKey, PrivateKey>;
  } {
    const symKey = SymmetricKey.generate();
    const encryptedGroupPrivateKey = symKey.encrypt(groupPrivateKey);

    const encryptedGroupAESKey = userPublicKey.encrypt(symKey);
    return {
      key_ciphertext: encryptedGroupAESKey,
      private_key_ciphertext: encryptedGroupPrivateKey
    };
  }

  /**
   * Encrypt the value of an object of secrets in key/value form
   * @param publicKey - the public key if any to use for the encryption
   * @param secrets - the secrets to encrypt
   * @return return an object with the new aes key and the encrypted secrets
   */
  private _encrypt_secrets(
    secrets: Map<string, UTF8Binary>,
    publicKey?: PublicKey
  ): [
    Encrypted<KeyPair, SymmetricKey>,
    Map<string, Encrypted<SymmetricKey, UTF8Binary>>
  ] {
    if (!publicKey) {
      publicKey = this.keyPair.publicKey;
    }
    let key = SymmetricKey.generate();
    const encryptedSecrets = mapKeys(secrets, s => key.encrypt(s));

    // encoding weirdness to preserve compat with old data
    key = new SymmetricKey(new UTF8Binary(key.string).binary);
    const encryptedKey = publicKey.encrypt(key);
    return [encryptedKey, encryptedSecrets];
  }

  /** Finish log in process */
  private async do_log_in(
    email: string,
    hash: string,
    password: string,
    expires?: number
  ) {
    const base = encodeAuthBase64(email + ":" + hash);
    const { token, user } = await this.api.login(base, expires);
    this.api.token = token;
    this.userId = user.id;
    const encrPrivKey = new Encrypted<Password, PrivateKey>(user.private_key);
    const privateKey = new Password(password).decrypt(encrPrivKey);

    this.keyPair = new KeyPair(new PublicKey(user.public_key), privateKey);
    return {
      user,
      token,
      privateKey: this.keyPair.privateKey.string
    };
  }
}
