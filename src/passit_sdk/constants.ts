export const BASE_URL = "http://localhost:8000";
export const API_URL = `${BASE_URL}/api/`;
export const DEFAULT_HASH_ITERATIONS = 24000;
